<?php
include('system.inc.php');

echo outformat($pdo->query('SELECT `host_id`, 
	`host_key`, 
	`host_ip`, 
	`host_nick`, 
	`host_geoip`, 
	`host_create`, 
	`host_lastupdate`, 
	`host_playercount` 
	FROM `hosts`
	WHERE `host_lastupdate` >= NOW() - INTERVAL 5 MINUTE
	ORDER BY `id` DESC'));