<?php
// Database settings
$dbtype = 'mysql';
$dbhost = 'localhost';
$dbname = 'test';
$dbuser = 'root';
$dbpass = 'QhY9h6JTERirLHG4';

// API settings
$interval = 10;	// How long in seconds the clients have to wait between making requests
$timeout = 10; // How many minutes it takes before a session is assumed down
$closed = false	// If true, tells clients to go elsewhere

// Advanced database settings
$dbcharset = 'utf8mb4';
$dsn = "$dbtype:host=$dbhost;dbname=$dbname;charset=$dbcharset"; // To change port, add ";port=##" at the end, and replace ## with the port number
$dbopt = [
	PDO::ATTR_ERRMODE				=>	PDO::ERRMODE_EXCEPTION,
	PDO::ATTR_DEFAULT_FETCH_MODE	=>	PDO::FETCH_ASSOC,
	PDO::ATTR_EMULATE_PREPARES		=>	false,
];