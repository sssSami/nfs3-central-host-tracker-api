<?php
if(!isset($inc)) die('No. Bad.');
include('config.inc.php');
header('Content-Type: text/json; Charset=UTF8');
try {
	$pdo = new PDO($dsn, $dbuser, $dbpass, $dbopt);
}
catch(Exception $err) {
	error_log('NFS3-Tracker: '.$err);
	$error = true;
}

function outformat((array) $data) {
	return json_encode($data,  JSON_PRETTY_PRINT |  JSON_FORCE_OBJECT);
}